#!/bin/bash

# Set the email address where you want to receive alerts
email="your@email.com"

# Set the threshold for certificate expiration in days
threshold=30

# Get the current date
current_date=$(date +%s)

# Loop through all the domain names
while read domain; do
  # Get the expiration date of the certificate
  expiration_date=$(echo | openssl s_client -servername "$domain" -connect "$domain:443" 2>/dev/null | openssl x509 -noout -dates | grep 'After' | cut -d'=' -f2)

  # If the expiration date is not available, skip this domain
  if [ -z "$expiration_date" ]; then
    continue
  fi

  # Convert the expiration date to a timestamp
  expiration_timestamp=$(date -d "$expiration_date" +%s)

  # Calculate the number of days until expiration
  days_until_expiration=$(( (expiration_timestamp - current_date) / 86400 ))

  # If the number of days until expiration is less than the threshold, send an alert
  if [ "$days_until_expiration" -lt "$threshold" ]; then
    echo "The SSL certificate for $domain will expire in $days_until_expiration days." | mail -s "SSL Certificate Expiration Alert" "$email"
  fi
done < domains.txt
